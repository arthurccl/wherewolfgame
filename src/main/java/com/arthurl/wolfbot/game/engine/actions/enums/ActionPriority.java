package com.arthurl.wolfbot.game.engine.actions.enums;

public enum ActionPriority {
    NOW,
    HIGH,
    MEDIUM,
    LOW,
    ASYNC,
}
